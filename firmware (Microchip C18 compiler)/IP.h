#define IPPROTO_ICMP    (1)
#define IPPROTO_UDP     (17)

typedef struct {
   u8    b[4];
} IPAddr;

typedef struct {
	u8		verlen;
	u8		typeOfService;
	u16		totalLength;
	u16		id;
	u16		fragmentInfo;
	u8		TTL;
	u8		protocol;
	u16		checksum;
	IPAddr	sourceIP;	
	IPAddr	destIP;	
} IP_Header;


typedef struct {
	IPAddr	sourceIP;
	IPAddr	destIP;
	u8		zero;
	u8		protocol;
	u16		length;
} Pseudo_Header;

extern IPAddr MyIP;
extern IPAddr destIP;
extern u16 destPortNumber;
                      
void processIP(void);
void IPPutHeader(IPAddr target, u8 protocol, u8* data, u16 datalen, u16 totalLength);
u8 ipMatch(IPAddr ip1, IPAddr ip2);
