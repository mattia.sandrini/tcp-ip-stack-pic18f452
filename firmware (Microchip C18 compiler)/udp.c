
#include "stack.h"

#define UDP_Offset	sizeof(IP_Header)

BOOL connesso;
BOOL blinkGiallo;


void processUDP(IP_Header ipHeader){
	UDPPacket	packet;
	u8 size;

	size = ipHeader.totalLength - (ipHeader.verlen & 0x0F) * 4;
	if (size > sizeof(packet)) size = sizeof(packet);      // se il campo data ha un numero di byte maggiore di MAX_UDP_DATA, scarta l'eccesso.

	encGetArray((u8*)&packet, size);
	
	if (packet.destPort == htons(MY_PORT_NUMBER))      // Se il pacchetto � indirizzato a me...   packet.destPort == MY_PORT_NUMBER   
	{
		if (packet.data[0] == 'b')
		{
			destIP = ipHeader.sourceIP;    // All'inzio della connessione col client ottengo il suo indirizzo IP
			destPortNumber = htons(packet.sourcePort);  // E salvo il numero di porta dell'applicazione client

			connesso = TRUE;
			PORTB = 1;

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == 'e')
		{
			blinkGiallo = FALSE;  // Fermo il led nel caso stia lampeggiando
			connesso = FALSE;
			PORTB = 0;            // Spengo tutti i led

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '1')
		{
			blinkGiallo = FALSE;
			PORTB = PORTB | 2;

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '2')
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);
			PORTB = PORTB | 4;

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '3')
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);
			PORTB = PORTB | 8;

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '4')  // Spengo il secondo led
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '5')  // Spengo il terzo led
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);
			PORTB = PORTB & (~4);

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '6')  // Spengo il quarto led
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);
			PORTB = PORTB & (~8);

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '7')  // Faccio lampeggiare il secondo led
		{
			blinkGiallo = TRUE;

			sendUDP(destPortNumber, packet.data, 1);
		}
		else if (packet.data[0] == '8')  // Fermo il secondo led
		{
			blinkGiallo = FALSE;
			PORTB = PORTB & (~2);

			sendUDP(destPortNumber, packet.data, 1);
		}
		else
		{
			sendUDP(destPortNumber, packet.data, 1);
		}
	}
}


void sendUDP(u16 destPort, u8 *data, u16 dataLen)
{
	UDPPacket packet;
	u16 totLen = 8 + dataLen;   // 8 byte di intestazione UDP, e dataLen byte di dati...
	u8 i=0;

	packet.sourcePort = htons(MY_PORT_NUMBER);
	packet.destPort = htons(destPort);
	packet.totLen = htons(totLen);			
	packet.checksum = 0;
	
	
	while (i < dataLen) {             // copio i dati da inviare in packet.data
		packet.data[i] = data[i];
		i++;
	}
	
	IPPutHeader(destIP, IPPROTO_UDP, (u8*)&packet, totLen, totLen);  // Scrivo nel buffer di trasmissione dell'ENC
																	 // l'intestazione MAC ed IP
	

	putChecksum(UDP_Offset + 6, 0);   // Disattivo il checksum
	
	MACSend();                        // Invio dei dati sul mezzo di trasmissione
}

