#include "stack.h"

void processARP(){
	ARPPacket packet;
	IPAddr tmp;	
	
	encGetArray((u8*)&packet, sizeof(packet));
	packet.operation = htons(packet.operation);
  
	if (packet.operation == ARP_REQUEST){
		if (ipMatch(packet.TargetIP,MyIP)){
			packet.operation = htons(ARP_REPLY);
			tmp = packet.TargetIP;
			packet.TargetMAC = packet.SourceMAC;
			packet.TargetIP = packet.SourceIP;
			packet.SourceIP = tmp;
			packet.SourceMAC.b[0] = MY_MAC1;
			packet.SourceMAC.b[1] = MY_MAC2;
			packet.SourceMAC.b[2] = MY_MAC3;
			packet.SourceMAC.b[3] = MY_MAC4;
			packet.SourceMAC.b[4] = MY_MAC5;
			packet.SourceMAC.b[5] = MY_MAC6;
			MACPutHeader(packet.TargetMAC, TYPE_ARP);
			encPutArray((u8*)&packet,sizeof(packet));
			MACSend();
		}
	}
}

