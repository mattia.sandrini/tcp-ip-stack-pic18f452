
#define TYPE_ARP    0x0806
#define TYPE_IP     0x0800


typedef struct {
  u8    b[6];
} MACAddr;

typedef struct {
  MACAddr       destMAC;
  MACAddr       sourceMAC;
  u16           type;
} MAC_Header;

