#include "stack.h" 

MACAddr	remoteAddr;

void processPacket(){
	MAC_Header header;
	setBank(1);
	while (readETH(EPKTCNT)) { // se c'� almeno un pacchetto

		MACGetHeader(&header);
		if (header.type == TYPE_IP){
			remoteAddr = header.sourceMAC;
			processIP();
		} else
		if (header.type == TYPE_ARP)
			processARP();

		freeRxSpace();  // libera lo spazio nel buffer RX
	}
}
