#include "stack.h"
#include <delays.h>

u16  bufSize;
u16  RdPt;
u16  packetStart;

void encInit(){
  	
  	TRISC = 0b10010000;  	// 8)RX;  7)TX;  6)SDO;  5)SDI;  4)SCK;  3) ;  2)CS;  1)RST;  
  	PORTC = 0x00;
	RST = 1;
  	SSPSTAT = 0b01000000;  							// 
  	SSPCON1 = 0b00100000;         					// configura il modulo SSP (SPI)
  	CS = 1;                 						// mette Chip Select a uno (non selezionato)										
  	PIR1bits.SSPIF = 0;      						// resetta il flag del modulo SPI						
	Delay100TCYx(1);								// startup
  	sendReset();									// resetta il NIC
  	Delay100TCYx(80);								// attende 1ms

  	//writePHY(PHLCON,0b0000101010110000);			// configurazione LED (per test)

  	// configura puntatori Buffer RX e TX
  	setBank(0);
  	writeReg(ERXSTL,   LOW(RX_BUF_START));			//
  	writeReg(ERXSTH,   HIGH(RX_BUF_START));			// inizio buffer di lettura
  	writeReg(ERXRDPTL, LOW(RX_BUF_END));			// 
  	writeReg(ERXRDPTH, HIGH(RX_BUF_END));			// puntatore del buffer di lettura
  	writeReg(ERXNDL,   LOW(RX_BUF_END));			//
  	writeReg(ERXNDH,   HIGH(RX_BUF_END));			// fine buffer di lettura
  	writeReg(ETXSTL,   LOW(TX_BUF_START));			//
  	writeReg(ETXSTH,   HIGH(TX_BUF_START));			// inizio buffer di scrittura

  	// configura il MAC
  	setBank(2);
  	writeReg(MACON1, 0b01101);  					// MARXEN  TXPAUS  RXPAUS
  	writeReg(MACON3, 0b00110000); 					// Half Duplex, Padding 60byte, CRC
  	writeReg(MAIPGL, 0x12); 						//
  	writeReg(MAIPGH, 0x0C); 						// 
  	writeReg(MABBIPG,0x12); 						// Inter-Packet Gap

  	// dimensione massima pacchetti
  	writeReg(MAMXFLL, LOW(1500));
  	writeReg(MAMXFLH, HIGH(1500));

  	// salva l'indirizzo MAC nel chip per il filtro
  	setBank(3);
  	writeReg(MAADR1, MY_MAC1);
  	writeReg(MAADR2, MY_MAC2);
  	writeReg(MAADR3, MY_MAC3);
  	writeReg(MAADR4, MY_MAC4);
  	writeReg(MAADR5, MY_MAC5);
  	writeReg(MAADR6, MY_MAC6); 

  	writePHY(PHCON2, 0b0000000100000000);			// disabilita il loopback
  	writePHY(PHCON1, 0);							// abilita il PHY

  	setBank(1);
  	writeReg(ERXFCON, 0b10100001); 					// imposta i filtri di ricezione
  	BFSReg(ECON1, 0b100);         					// abilita la ricezione

	writeReg(EIR, 0);
	writeReg(EIE, 0b11000000);						// abilita l'interrupt RX

	RdPt = 0;

	MyIP.b[0] = MY_IP1;
	MyIP.b[1] = MY_IP2;
	MyIP.b[2] = MY_IP3;
	MyIP.b[3] = MY_IP4;								// carica la variabile MyIP con l'IP locale	
	
/*		
	destIP.b[0] = 192;
	destIP.b[1] = 168;
	destIP.b[2] = 1;
	destIP.b[3] = 10;

	destPortNumber = 50000;
*/
}


u16 DMAChecksum(u16 start, u16 len, BOOL rx){
	// calcola il checksum utilizzando l'apposita funzione del chip
	u16 tmp;
	u8 L,H;
	if (rx) {
    	tmp = TX_BUF_START + 1 + sizeof(MAC_Header) + start;
	} else {
		tmp = packetStart + 6 + sizeof(MAC_Header) + start;
		if (tmp > RX_BUF_END)
			tmp = tmp - RX_BUF_END + RX_BUF_START - 1;
	}

	setBank(0);
	writeReg(EDMASTL, LOW(tmp));
	writeReg(EDMASTH, HIGH(tmp));
	
	tmp = tmp+len-1; 					// fine pacchetto
	if (!rx && tmp > RX_BUF_END)
		tmp = tmp - RX_BUF_END + RX_BUF_START - 1;
	writeReg(EDMANDL, LOW(tmp));
	writeReg(EDMANDH, HIGH(tmp));
	
	BFSReg(ECON1, 0b00110000);      	// inizio calcolo
	while(readETH(ECON1) & 0b00100000); // attende fine calcolo
	tmp = (u16)readETH(EDMACSL) << 8;
	tmp = tmp | readETH(EDMACSH);
	return tmp;							// ritorna il checksum calcolato
}


void putChecksum(u16 offset, u16 sum){
	// scrive il checksum "sum" all'indirizzo "offset" all'interno del buffer di scrittura
	u16 tmp;
	u16 addr[2];                         
	setBank(0);		
	addr[0] = readETH(EWRPTL);			// salva temporaneamente il puntatore
	addr[1] = readETH(EWRPTH);			// di scrittura
	tmp = 1+sizeof(MAC_Header)+offset;	// nuovo indirizzo 
	writeReg(EWRPTL,LOW(TX_BUF_START+tmp));
	writeReg(EWRPTH,HIGH(TX_BUF_START+tmp));	// carica il puntatore 
	encPut(LOW(sum));
	encPut(HIGH(sum));      			// scrive il checksum
	writeReg(EWRPTL,addr[0]);
	writeReg(EWRPTH,addr[1]);			// ripristina il vecchio puntatore	
}

void encDiscard(u16 len){     // Scarta il pacchetto
	u16 addr;
	setBank(0);
	addr = readETH(ERDPTL);
	addr = ((u16)readETH(ERDPTH)<<8) | addr;
	addr += len;
	writeReg(ERDPTL,LOW(addr));
	writeReg(ERDPTH,HIGH(addr));
}

void MACGetHeader(MAC_Header* header){
	u8 buf[6];

	packetStart = RdPt;								// salva RdPt in packetStart
	setBank(0);
	writeReg(ERDPTL, LOW(RdPt));					//
	writeReg(ERDPTH, HIGH(RdPt));					// ERDPT = RdPt
	encGetArray(&buf[0], 6);						// legge i 6 byte di controllo
	RdPt = (u16)((u16)buf[0] | ((u16)buf[1] << 8)); // puntatore prossimo pacchetto
       
	encGetArray((u8*)header, sizeof(MAC_Header));	// legge l'intestazione MAC
       
	header->type = htons(header->type);				// swappa il campo type
}


void freeRxSpace(){
	u16 NewRdPt;
	setBank(0);  
	NewRdPt = RdPt -1;
	if ((NewRdPt > RX_BUF_END) || (NewRdPt < RX_BUF_START)) NewRdPt = RX_BUF_END;
	BFSReg(ECON2,0b01000000);    		// decrementa EPKTCNT
	writeReg(ERXRDPTL, LOW(NewRdPt));
	writeReg(ERXRDPTH, HIGH(NewRdPt)); 	// libera lo spazio del buffer
}


void MACPutHeader(MACAddr target, u16 type){
	u8 i;
	bufSize = sizeof(MAC_Header);		
	setBank(0);
	writeReg(EWRPTL, LOW(TX_BUF_START));
	writeReg(EWRPTH, HIGH(TX_BUF_START));
	CS = 0;
	spiWrite(WBM);
	spiWrite(0x00);	// usa MACON3

	for (i=0;i<6;i++)
		spiWrite(target.b[i]);
	spiWrite(MY_MAC1);
	spiWrite(MY_MAC2);
	spiWrite(MY_MAC3);
	spiWrite(MY_MAC4);
	spiWrite(MY_MAC5);
	spiWrite(MY_MAC6);

	spiWrite(HIGH(type));
	spiWrite(LOW(type));
	CS = 1;
}

void MACSend(){  
	setBank(0);
	if (readETH(EIR) & 0b10) {
		BFSReg(ECON1, 0b10000000);
		BFCReg(ECON1, 0b10000000);
	}
	while(readETH(ECON1) & 0b1000);  //aspetta che sia pronto ad inviare
	writeReg(ETXNDL, LOW(TX_BUF_START + bufSize));
	writeReg(ETXNDH, HIGH(TX_BUF_START + bufSize));
	BFSReg(ECON1, 0b1000); // invia
}

u8	isMACLinked(){ 						
	// restituisce 1 se il cavo � collegato
	return ((readPHY(PHSTAT1) & 0b100));
}

void encGetArray(u8* buf, u16 len){
	CS = 0;
	spiWrite(RBM);
	while(len--) 
		*buf++ = spiRead();
	CS = 1;
}

void encPutArray(u8* buf,u16 len){
	bufSize += len;
	CS = 0;
	spiWrite(WBM);  
	while(len--) 
		spiWrite(*buf++);    
	CS = 1;
}	   

void encPutString(const rom u8 *str){
	CS = 0;
	spiWrite(WBM);  
	while(*str) {
    	spiWrite(*str++);     
		bufSize++;
	}
	CS = 1;
}

void encPut(u8 b){  
	CS = 0;
	spiWrite(WBM);
	spiWrite(b);
	CS = 1;	
	bufSize++;
}

u8 encGet(){  
	u8 b;
	CS = 0;
	spiWrite(RBM);
	b = spiRead();
	CS = 1;	
	return b;
}

void sendReset(){
	CS = 0;
	spiWrite(SRC);
	CS = 1;
}

void writeReg(u8 reg, u8 data){
	CS = 0;
	spiWrite(WCR | reg);
	spiWrite(data);
	CS = 1;
}

void writePHY(u8 reg, u16 data){
	setBank(2);
	writeReg(MIREGADR,reg);
	writeReg(MIWRL,LOW(data));
	writeReg(MIWRH,HIGH(data));

	setBank(3);
	while (readMAC(MISTAT) & 1);
}

u16 readPHY(u8 reg){
	setBank(2);
	writeReg(MIREGADR,reg);
	writeReg(MICMD, 0x01);	

	setBank(3);
	while(readMAC(MISTAT) & 1);

	setBank(2);
	writeReg(MICMD, 0x00);	
	
	return readMAC(MIRDL) | (readMAC(MIRDH) << 8);
}

u8 readMAC(u8 reg){
	u8 b;
	CS = 0;
	spiWrite(RCR | reg);
	spiRead();
	b = spiRead();
	CS = 1;
	return b;
}

u8 readETH(u8 reg){
	u8 b;
  	CS = 0;
	spiWrite(RCR | reg);
	b = spiRead();
  	CS = 1;
  	return b;
}

void BFCReg(u8 reg, u8 data){
  	CS = 0;
	spiWrite(BFC | reg);
	spiWrite(data);
  	CS = 1;
}

void BFSReg(u8 reg, u8 data){
  	CS = 0;
	spiWrite(BFS | reg);
	spiWrite(data);
  	CS = 1;
}

void setBank(u8 bank){
  	BFCReg(ECON1,0b11);
  	BFSReg(ECON1,bank);
}

u8 spiRW(u8 data){
	SSPBUF = data;
	while(!PIR1bits.SSPIF);
	PIR1bits.SSPIF = 0;
	return SSPBUF;
}
