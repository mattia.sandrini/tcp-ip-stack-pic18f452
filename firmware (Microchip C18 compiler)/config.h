#pragma config OSC = HSPLL
#pragma config PWRT = ON
#pragma config WDT = OFF
#pragma config LVP = OFF
#pragma config DEBUG = OFF

/*
#pragma config OSC = HSPLL
#pragma config FCMEN = OFF
#pragma config IESO = OFF
#pragma config PWRT = ON
#pragma config BOREN = OFF 
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config LPT1OSC = OFF
#pragma config PBADEN = OFF
#pragma config CCP2MX = PORTC
#pragma config STVREN = OFF
#pragma config LVP = OFF
#pragma config XINST = OFF 
#pragma config DEBUG = OFF
*/