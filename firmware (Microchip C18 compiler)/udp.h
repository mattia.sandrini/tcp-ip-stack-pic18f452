
#define MAX_UDP_DATA 	16



void processUDP(IP_Header ipHeader);
void sendUDP(u16 destPort, u8 *data, u16 dataLen);




typedef struct {
	u16	sourcePort;
	u16	destPort;
	u16	totLen;
	u16	checksum;
	u8	data[MAX_UDP_DATA];
} UDPPacket;


extern BOOL connesso;
extern BOOL blinkGiallo;
