
#include "config.h"
#include "stack.h"
#include <usart.h>

/*
void initUSART()
{
	 Registro USART Baud Rate Generator (BRG)

BRGH=0 (low speed)                                      BRGH=1 (high speed)

(asincrona)Baud Rate=Fosc/(64(x+1))                    Baud rate=Fosc/(16(x+1))

x= valore in SPBRG (0-255) Fosc= frequenza del clock del microcontrollore

32 MHz:
9600 = 32000000/(64*(x+1))   --->  x=51                 9600 = 32000000/(16*(x+1))   --->  x= 


8 MHz:
9600 = 8000000/(64*(x+1))   --->  x=12




	OpenUSART(USART_TX_INT_OFF &
		  	  USART_RX_INT_OFF &
		  	  USART_ASYNCH_MODE &
		  	  USART_EIGHT_BIT &
		  	  USART_CONT_RX &
		  	  USART_BRGH_LOW,
		  	  51);               // Baud rate 9600, 32 MHz, USART_BRGH_LOW
}*/




void main() {
	u8 i = 0, j = 0;
	u8 datiRFID[12]; 
	u8 tempByte = 0;
	u16 blink = 0;

	TRISB = 0x00;        						// configurazione I/O di PORTB
	PORTB = 0;
	connesso = FALSE;
	blinkGiallo = FALSE;

	encInit();

	OpenUSART(USART_TX_INT_OFF &
		  	  USART_RX_INT_OFF &
		  	  USART_ASYNCH_MODE &
		  	  USART_EIGHT_BIT &
		  	  USART_CONT_RX &
		  	  USART_BRGH_LOW,
		  	  51);               // Baud rate 9600, 32 MHz, USART_BRGH_LOW
	
	while (1)
	{
		processPacket();
		
		
		if (DataRdyUSART())
		{
			tempByte = ReadUSART();

			if (tempByte == 2)      // Controllo se è il byte delimitatore di inizio
			{                  
     			i = 0;
     			while (i < 12)      // Legge i 10 byte di codice + 2 per il checksum
				{                        
       				if (DataRdyUSART()) 
					{
         				tempByte = ReadUSART();


						// Se non è un byte significativo
         				if ((tempByte == 0x0D) ||  // 13   (CR)
							(tempByte == 0x0A) ||  // 10   (LF)
							(tempByte == 0x03) ||  // ETX
							(tempByte == 0x02))    // STX
						{
						   break;                                    // Non lo registro
         				}

						datiRFID[i] = tempByte;

         				i++;                                // Preparo l'indice per registrare il prossimo byte
       				}
     			}

     			// Invio in rete tramite protocollo UDP i dati letti

     			if (i == 12 && connesso)   // Se sono stati letti i 12 byte ed il client è in ascolto di eventuali pacchetti
       			{
					sendUDP(destPortNumber, datiRFID, 12);
     			}

     			i = 0;
   			}	
		}	

		if (blinkGiallo)   // Nel caso sia stato attivato il comando per il lampeggiamento del led giallo
		{
			if (blink == 0)
				PORTB = PORTB | 2;   // Accendo il giallo
			else if (blink == 15000)
				PORTB = PORTB & (~2);   // Spengo il giallo

			blink++;   // Incremento il contatore per il lampeggiamento del led

			if (blink > 30000)
				blink = 0;     // Azzero il contatore
		}
		else 
			blink = 0;
	}
}










