// configurazione del buffer TX/RX
#define RX_BUF_START    0
#define RX_BUF_END 	    6499	
#define	TX_BUF_START    6500

// indirizzo MAC
#define MY_MAC1         0x00
#define MY_MAC2         0x04
#define MY_MAC3         0xA3
#define MY_MAC4         0x00
#define MY_MAC5         0x00
#define MY_MAC6         0x00

// indirizzo IP
#define MY_IP1          192
#define MY_IP2          168
#define MY_IP3          1
#define MY_IP4          13

#define MY_PORT_NUMBER  33000

#define	CS          PORTCbits.RC1		// CS dell'ENC
#define	RST         PORTCbits.RC0
#define LOW(x)      (u8)((u16)x & 0xFF)
#define HIGH(x)     (u8)((u16)x >> 8)

#define TRUE		1
#define FALSE		0



typedef unsigned char u8;
typedef unsigned int  u16;
typedef unsigned long u32;
typedef u8 BOOL;





