# TCP/IP stack for PIC18F452 micro Controller

Implementation of the TCP/IP stack on a PIC18F452 micro-controller enable to route UDP packets in a local network through a ENC28J60 module. The purpose of this system consists in observing RFID transponders presence within the RFID reader field and recording these events on a Java app's database.

# Setup

Open the windows comand line and run these comands:

1. Create the database structure
   > mysql>source db-setup.sql

2. Go to the directory containing the program myodbc-installer.exe
   > cd "C:\Programmi\MySQL\Connector ODBC 5.1"

3. Register the ODBC driver
   > myodbc-installer.exe -d -a -n "MySQL ODBC 5.1 Driver" -t "DRIVER=myodbc5.dll;SETUP=myodbc5S.dll"

4. Create a new data origin
   > myodbc-installer.exe -s -a -c1 -n "origineDati" -t "DRIVER=MySQL ODBC 5.1 Driver;SERVER=localhost;DATABASE=tesina;UID=root;PWD="

5. Print the information about the previously created data origin
   > myodbc-installer.exe -s -l -c1 -n "origineDati"


# Electrical Schematic

![Electrical Schematic](electrical%20schematic/Electrical%20Schematic.png)


# Demonstrative video

[![Demonstrative video](https://img.youtube.com/vi/H9lK97t_Lc8/0.jpg)](http://www.youtube.com/watch?v=H9lK97t_Lc8)
