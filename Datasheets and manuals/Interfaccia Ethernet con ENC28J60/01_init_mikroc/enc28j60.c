

u16  RdPt;
u16  packetStart;
u16  bufSize;

void encInit(){
	TRISB = 0x01;        						// configurazione I/O di PORTB
	TRISC = 0xD1;           						// configurazione I/O di PORTC
	PORTC = 0x00;
	
	RdPt = 0;

	SSPSTAT = 0x40;  								//
	SSPCON1 = 0x20;         						// configura il modulo SSP (SPI)
	CS = 1;                 						// mette Chip Select a uno (non selezionato)
	CS2 = 1;
	PIR1.SSPIF = 0;      				        	// resetta il flag del modulo SPI
  	
	sendReset();           							// resetta il NIC
	Delay_cyc(800);									// attende 1ms

	//WritePHY(PHLCON,0b0000101010110000);			// configurazione LED  (per test)

	// configura puntatori Buffer RX e TX
	setBank(0);
	writeReg(ERXSTL,   LOW(RX_BUF_START));			//
	writeReg(ERXSTH,   HIGH(RX_BUF_START));   		// inizio buffer di lettura
	writeReg(ERXRDPTL, LOW(RX_BUF_END));			//
	writeReg(ERXRDPTH, HIGH(RX_BUF_END));			// puntatore del buffer di lettura
	writeReg(ERXNDL,   LOW(RX_BUF_END));			//
	writeReg(ERXNDH,   HIGH(RX_BUF_END));			// fine buffer di lettura
	writeReg(ETXSTL,   LOW(TX_BUF_START));			//
	writeReg(ETXSTH,   HIGH(TX_BUF_START));			// inizio buffer di scrittura

	// configura il MAC
	setBank(2);
	writeReg(MACON1, 0b01101);  			// MARXEN  TXPAUS  RXPAUS
	writeReg(MACON3, 0b00110000); 			// Half Duplex, Padding 60byte, CRC
	writeReg(MAIPGL, 0x12); 				//
	writeReg(MAIPGH, 0x0C); 				//
	writeReg(MABBIPG,0x12); 				// Inter-Packet Gap

	// dimensione massima pacchetti
	writeReg(MAMXFLL, LOW(1500));
	writeReg(MAMXFLH, HIGH(1500));

	// salva l'indirizzo MAC nel chip per il filtro
	setBank(3);
	writeReg(MAADR1, MY_MAC1);
	writeReg(MAADR2, MY_MAC2);
	writeReg(MAADR3, MY_MAC3);
	writeReg(MAADR4, MY_MAC4);
	writeReg(MAADR5, MY_MAC5);
	writeReg(MAADR6, MY_MAC6);

	writePHY(PHCON2, 0b0000000100000000);		// disabilita il loopback
	writePHY(PHCON1, 0);						// abilita il PHY

	setBank(1);
	writeReg(ERXFCON, 0b10100001); 				// imposta i filtri di ricezione
	BFSReg(ECON1, 0b100);         				// abilita la ricezione

	writeReg(EIR, 0);
	writeReg(EIE, 0b11000000);					// abilita l'interrupt RX
}

u8	isMACLinked(){  // restituisce 1 se il cavo � collegato
	return ((readPHY(PHSTAT1) & 0b100));
}

void encGetArray(u8* buf, u16 len){
	CS = 0;
	spiWrite(RBM);
	while(len--) 
		*buf++ = spiRead();
	CS = 1;
}

void encPutArray(u8* buf,u16 len){
	bufSize += len;
	CS = 0;
	spiWrite(WBM);  
	while(len--) 
		spiWrite(*buf++);    
	CS = 1;
}	   

void encPutString(const u8 *str){
	CS = 0;
	spiWrite(WBM);  
	while(*str) {
    	spiWrite(*str++);     
		bufSize++;
	}
	CS = 1;
}

void encPut(u8 b){  
	CS = 0;
	spiWrite(WBM);
	spiWrite(b);
	CS = 1;	
	bufSize++;
}

u8 encGet(){  
	u8 b;
	CS = 0;
	spiWrite(RBM);
	b = spiRead();
	CS = 1;	
	return b;
}

void sendReset(){
	CS = 0;
	spiWrite(SRC);
	CS = 1;
}

void writeReg(u8 reg, u8 data){
	CS = 0;
	spiWrite(WCR | reg);
	spiWrite(data);
	CS = 1;
}


void writePHY(u8 reg, u16 data){
	setBank(2);
	writeReg(MIREGADR,reg);
	writeReg(MIWRL,LOW(data));
	writeReg(MIWRH,HIGH(data));

	setBank(3);
	while (readMAC(MISTAT) & 1);
}

u16 readPHY(u8 reg){
	setBank(2);
	writeReg(MIREGADR,reg);
	writeReg(MICMD, 0x01);	

	setBank(3);
	while(readMAC(MISTAT) & 1);

	setBank(2);
	writeReg(MICMD, 0x00);	
	
	return readMAC(MIRDL) | (readMAC(MIRDH) << 8);
}

u8 readMAC(u8 reg){
	u8 b;
	CS = 0;
	spiWrite(RCR | reg);
	spiRead();
	b = spiRead();
	CS = 1;
	return b;
}

u8 readETH(u8 reg){
	u8 b;
  	CS = 0;
	spiWrite(RCR | reg);
	b = spiRead();
  	CS = 1;
  	return b;
}

void BFCReg(u8 reg, u8 data){
  	CS = 0;
	spiWrite(BFC | reg);
	spiWrite(data);
  	CS = 1;
}

void BFSReg(u8 reg, u8 data){
  	CS = 0;
	spiWrite(BFS | reg);
	spiWrite(data);
  	CS = 1;
}

void setBank(unsigned char bank){
  	BFCReg(ECON1,0b11);
  	BFSReg(ECON1,bank);
}

u8 spiRW(u8 data){
	SSPBUF = data;
	while(!PIR1.SSPIF);
	PIR1.SSPIF = 0;
	return SSPBUF;
}
