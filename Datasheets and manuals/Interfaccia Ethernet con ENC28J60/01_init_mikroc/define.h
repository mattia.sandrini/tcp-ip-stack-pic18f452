// configurazione del buffer TX/RX
#define RX_BUF_START 0
#define RX_BUF_END 	 6499	
#define	TX_BUF_START 6500

// indirizzo MAC
#define MY_MAC1     0x00
#define MY_MAC2     0x04
#define MY_MAC3     0xA3
#define MY_MAC4     0x00
#define MY_MAC5     0x00
#define MY_MAC6     0x00

// indirizzo IP
#define MY_IP1      10
#define MY_IP2      0
#define MY_IP3      0
#define MY_IP4      5

#define	CS          PORTC.F2		// CS dell'ENC
#define	CS2	    PORTC.F1		// CS della memoria SPI (se presente)
#define LOW(x)      (u8)((u16)x & 0xFF)
#define HIGH(x)     (u8)((u16)x >> 8)

#define TRUE		1
#define FALSE		0

typedef unsigned char u8;
typedef u8 BOOL;
typedef unsigned int  u16;
typedef unsigned long u32;





