
#define ICMP_ECHO		8
#define ICMP_ECHO_REPLY	0
#define MAX_ICMP_DATA 	32

typedef struct {
	u8	type;
	u8	code;
	u16	checksum;
	u16	id;
	u16	sn;
	u8	data[MAX_ICMP_DATA];
} ICMPPacket;

void processICMP(IP_Header ipHeader);


