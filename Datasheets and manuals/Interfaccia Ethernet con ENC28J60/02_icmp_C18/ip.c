#include "stack.h"

#define	IP_Offset	0


extern MACAddr	remoteAddr;

IPAddr MyIP;
u16 id = 0;

u8 ipMatch(IPAddr ip1, IPAddr ip2){
	return (ip1.b[0] == ip2.b[0] && ip1.b[1] == ip2.b[1] && ip1.b[2] == ip2.b[2] && ip1.b[3] == ip2.b[3]);
}

void swapIPHeader(IP_Header* header){
	header->totalLength = htons(header->totalLength);
	header->id = htons(header->id);
	header->fragmentInfo = htons(header->fragmentInfo);
}

void processIP(){
	IP_Header header;
	u16 chksum;
	u8 optlen;		

	encGetArray((u8*)&header, sizeof(header));

	if (!ipMatch(header.destIP,MyIP)) return;
	
	// controlla il checksum
	if (DMAChecksum(0, (header.verlen & 0x0F)*4, FALSE)) return;
	
	// scarta eventuali campi option
	optlen = (header.verlen & 0x0F)*4 - 20;	
	if (optlen)
		encDiscard(optlen);
		
	// big endian --> little endian
	swapIPHeader(&header);

	switch (header.protocol){
		case IPPROTO_ICMP: processICMP(header);	 
			break;
	/*	case IPPROTO_UDP : 
			break;
		case IPPROTO_TCP: 
			break;	*/			
		default: break;
	}		
}


void IPPutHeader(IPAddr target, u8 protocol, u8* data, u16 datalen, u16 totalLength){
	IP_Header header;
	u16 tmp;
	header.verlen = 0x45;
	header.typeOfService = 0x00;
	header.totalLength = 20+totalLength;
	header.id = ++id;
	header.fragmentInfo = 0x00;
	header.TTL = 128;
	header.protocol = protocol;
	header.checksum = 0x00;;
	header.sourceIP = MyIP;	
	header.destIP = target; 
	
	swapIPHeader(&header);
		
	MACPutHeader(remoteAddr, TYPE_IP);
	encPutArray((u8*)&header, sizeof(header));
	encPutArray(data, datalen);  
	putChecksum(IP_Offset+10, DMAChecksum(IP_Offset, sizeof(header), TRUE));
}

