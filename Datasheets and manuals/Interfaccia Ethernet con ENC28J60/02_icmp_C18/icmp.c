#include "stack.h"

#define ICMP_Offset	sizeof(IP_Header)

void processICMP(IP_Header ipHeader){
	ICMPPacket	packet;
	u8 size;              

	size = ipHeader.totalLength - (ipHeader.verlen & 0x0F)*4;
	if (size > sizeof(packet)) size = sizeof(packet);
	encGetArray((u8*)&packet, size);
	
	if (packet.type == ICMP_ECHO){
		packet.type = ICMP_ECHO_REPLY;
		packet.checksum = 0;
		IPPutHeader(ipHeader.sourceIP, IPPROTO_ICMP, (u8*)&packet, size, size);
		putChecksum(ICMP_Offset+2,DMAChecksum(ICMP_Offset,size,TRUE));
		MACSend();
	}
	
}
