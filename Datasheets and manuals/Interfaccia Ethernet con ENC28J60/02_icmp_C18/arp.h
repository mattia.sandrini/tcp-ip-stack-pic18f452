#define ARP_REQUEST     1
#define ARP_REPLY       2

typedef struct {
  u16          hardware;
  u16          protocol;
  u8           MacLen;
  u8           ProtocolLen;
  u16          operation;
  MACAddr      SourceMAC;
  IPAddr       SourceIP;
  MACAddr      TargetMAC;
  IPAddr       TargetIP;
} ARPPacket;

void processARP(void); 


