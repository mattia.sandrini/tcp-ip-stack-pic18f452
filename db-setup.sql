create database tesina;
use tesina;

create table utenti (
id_utente int(10) auto_increment primary key,
tag char(12),
nome varchar(50),
cognome varchar(50),
codFiscale varchar(50),
indirizzo varchar(50)
);


create table passaggi (
id_passaggio int(10) auto_increment primary key,
tag char(12),
id_utente int(10),
ora time,
data_passaggio date,
foreign key(id_utente) references utenti(id_utente)
);


