
package tesina;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class AggiungiUtente extends JFrame implements WindowListener{
    JPanel homePanel;
    JButton add;
    JLabel[] etichette = new JLabel[4];
    JTextArea[] text = new JTextArea[4];
    
    
    public AggiungiUtente(ArrayList rs) {
        Globali.rsTemp = rs;
        this.setBounds(500,300,200,200);
        this.setTitle("RFID");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("icona.png"));
        
        homePanel = new JPanel();
        homePanel.setLayout(new GridLayout(5,2));
        
        etichette = new JLabel[4];
        etichette[0] = new JLabel("Nome: ");
        etichette[1] = new JLabel("Cognome: ");
        etichette[2] = new JLabel("Codice Fiscale: ");
        etichette[3] = new JLabel("Indirizzo: ");
        text = new JTextArea[4];
        
        for(int i = 0; i < 4; i++){
            
            text[i] = new JTextArea();
            text[i].setBorder(BorderFactory.createLineBorder(Color.black));
            homePanel.add(etichette[i]);
            homePanel.add(text[i]);
        }
        
        add = new JButton("Aggiungi");
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String query = "INSERT INTO Utenti(id_utente,tag,nome,cognome,codFiscale,indirizzo) VALUES (" +
                                Globali.db.calcolaId("utenti", "id_utente") + ",'" + 
                                Globali.rsTemp.get(1) + "','" + 
                                text[0].getText() + "','" + 
                                text[1].getText() + "','" +
                                text[2].getText() + "','" +
                                text[3].getText() + "');";
                
                Globali.db.eseguiQuery(query);
                Globali.db.refreshDataBase((String)Globali.rsTemp.get(1));
                if(Globali.comunicazione!=null){
                    Globali.comunicazione.illuminaVerde(1000);
                    Globali.finestra.pcom.clickOnConnetti();}
                else
                    Globali.vp.tab.ordina(0);
                chiudi();
                
            }
        });
        homePanel.add(add);
        homePanel.add(new JLabel());
        
        this.getContentPane().add(homePanel);
        this.setVisible(true);
    }
    public void chiudi(){
        this.dispose();
    }

    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosing(WindowEvent e) {
        if(Globali.comunicazione.connesso)
            Globali.comunicazione.illuminaRosso(1000);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        
    }
}
