
package tesina;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DataBase {
    private Connection con;
    private Statement st;
    
    public DataBase(){
        try{
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        }catch(ClassNotFoundException e){System.out.println(e.getMessage());}
        try{
            con = DriverManager.getConnection("Jdbc:odbc:origineDati");
            st = con.createStatement();
        }catch(SQLException e){System.out.println(e.getMessage());}
    }
    public ArrayList aggiungiPassaggio(String messaggio){
        Globali.flag = false;
        ArrayList risultatoRicerca = ricerca(messaggio);
        String query;
        int id_passaggio = calcolaId("passaggi", "id_passaggio");
        
        
        if(risultatoRicerca.size() == 0)
            query = "INSERT INTO Passaggi(id_passaggio,ora,data_passaggio,tag) VALUES ("+id_passaggio+",CURTIME(),CURDATE(),'" + messaggio + "');";
        else
            query = "INSERT INTO Passaggi(id_passaggio,ora,data_passaggio,tag,id_utente) VALUES ("+id_passaggio+",CURTIME(),CURDATE(),'" + messaggio + "','"+risultatoRicerca.get(0)+"');";
        
        ResultSet rs = eseguiQuery(query);
        
        if(risultatoRicerca.size() == 0) {
            query = "SELECT id_passaggio,tag,ora,data_passaggio FROM Passaggi WHERE id_passaggio="+id_passaggio+";";
            rs = eseguiQuery(query);
            try {
                Globali.flag = true;
                rs.next();             // Sposto l'indice alla prima voce del resultset
                risultatoRicerca.add(rs.getString("id_passaggio"));
                risultatoRicerca.add(rs.getString("tag"));
                risultatoRicerca.add(rs.getString("ora"));
                risultatoRicerca.add(rs.getString("data_passaggio"));
            }catch(SQLException e){}
        }    
          
         
       
        return risultatoRicerca;//Restituisco l'arrayList risultato della ricerca del tag nella tabella utente
    }
    
    public int calcolaId(String tab, String attributo){
        int ris = 0;
        String query = "SELECT MAX("+attributo+") as 'id' FROM "+tab+";";
        ResultSet rs = eseguiQuery(query);
        try{
            while(rs.next())
            {
                ris = Integer.parseInt(rs.getString("id"));
            }
        }catch(SQLException  e){}
        return ris+1;
    }
    
    private ArrayList ricerca(String messaggio){
        ArrayList ris = new ArrayList();
        ResultSet rs = null;
        String query = "SELECT id_utente,codFiscale,nome,cognome,tag,indirizzo FROM Utenti WHERE tag = '"+messaggio+"';";
        rs = eseguiQuery(query);
        if(rs!=null){
            try{
                if(rs.next()){
                    ris.add(rs.getString("id_utente"));
                    ris.add(rs.getString("codFiscale"));
                    ris.add(rs.getString("Nome"));
                    ris.add(rs.getString("Cognome"));
                    ris.add(rs.getString("tag"));
                    ris.add(rs.getString("indirizzo"));
                }
            }catch(SQLException e){ }
        }
        return ris;
    }
    
    public ResultSet eseguiQuery(String query){
        ResultSet rs = null;
        try{
            rs = st.executeQuery(query);
        }catch(SQLException e){ }
        return rs;
    }
    
    public void refreshDataBase(String tag) {
        String id_utente = null;
        String query = "SELECT id_utente FROM Utenti WHERE tag='"+tag+"';";
        ResultSet rs = eseguiQuery(query);
        try{
            rs.next();
            id_utente = rs.getString("id_utente");
            query = "UPDATE Passaggi SET id_utente='"+id_utente+"' WHERE tag='"+tag+"';";
            eseguiQuery(query);
        }catch(SQLException e){}
    }
    
    
    public void refreshDataBase() {
        String id_utente = null;
        String tag;
        
        String query = "SELECT DISTINCT tag FROM passaggi WHERE id_utente IS NULL;";
        ResultSet rs = eseguiQuery(query);
        try{
            while (rs.next()) {
                tag = rs.getString("tag");
                
                query = "SELECT id_utente FROM Utenti WHERE tag='"+tag+"';";
                ResultSet rsUtenti = eseguiQuery(query);
                try{
                    rsUtenti.next();
                    id_utente = rsUtenti.getString("id_utente");
                    
                    query = "UPDATE passaggi SET id_utente='"+id_utente+"' WHERE tag='"+tag+"';";
                    eseguiQuery(query);
                } catch(SQLException e){}
            }
        }catch(SQLException e){}
    }
}

