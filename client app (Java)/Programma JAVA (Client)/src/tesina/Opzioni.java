
package tesina;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class Opzioni extends JFrame{
    JPanel homePanel;
    JLabel[] etichette;
    JTextArea[] text;
    JButton save;
    JButton ripristina;
    
    
    
    public Opzioni(){
        etichette = new JLabel[3];
        this.setBounds(500,300,350,200);
        this.setTitle("Opzioni parametri comunicazione");
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("icona.png"));
        
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        homePanel = new JPanel();
        homePanel.setLayout(new GridLayout(4,2));
        
        etichette = new JLabel[4];
        etichette[0] = new JLabel("My port number: ");
        etichette[1] = new JLabel("Server port number: ");
        etichette[2] = new JLabel("Ip server: ");
        text = new JTextArea[3];
        
        for(int i = 0; i < 3; i++){
            
            text[i] = new JTextArea();
            text[i].setBorder(BorderFactory.createLineBorder(Color.black));
            homePanel.add(etichette[i]);
            homePanel.add(text[i]);
        }
        
        text[0].setText(Globali.myPortNumber+"");
        text[1].setText(Globali.serverPortNumber+"");
        text[2].setText(Globali.serverIP.getHostAddress());
        
        save = new JButton("Salva");
        save.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int myPortNumber = 0;
                int serverPortNumber = 0;
                InetAddress serverIp = null;
                try{
                    myPortNumber = Integer.parseInt(text[0].getText());
                    serverPortNumber = Integer.parseInt(text[1].getText());
                    try{
                        serverIp = InetAddress.getByName(text[2].getText());
                    }catch(UnknownHostException ec) {JOptionPane.showMessageDialog(null,"Errore");ripristina();}
                }catch(Exception ecc){JOptionPane.showMessageDialog(null,"Errore");ripristina();};
                    Globali.myPortNumber = myPortNumber;
                    Globali.serverPortNumber = serverPortNumber;
                    Globali.serverIP = serverIp;
                    chiudi();
            
            }
        });
        homePanel.add(save);
        
        ripristina = new JButton("Rispristina");
        ripristina.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ripristina();
                chiudi();
            }
        });
        homePanel.add(ripristina);
        
        this.add(homePanel);
        
        this.setVisible(true);
    }
    public void ripristina(){
        Globali.myPortNumber = 50000;
        Globali.serverPortNumber = 33000;
        try{
        Globali.serverIP = InetAddress.getByName("192.168.1.13");
        }catch(UnknownHostException eccc){}
    }
    public void chiudi(){
        
        this.dispose();
    }
}
