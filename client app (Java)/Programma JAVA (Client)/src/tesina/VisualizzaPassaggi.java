
package tesina;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class VisualizzaPassaggi extends JFrame{
    public Tabella tab;
    private JLabel[] nomeColonne = new JLabel[4];
    private String query;
    private ResultSet rs;
    private JScrollPane scrollBar;
    private int[] temp = {1,1,1,1};
    private Container c;
    Color col = Color.blue;
    public VisualizzaPassaggi(){
        this.setBounds(200,200,700,500);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setTitle("Tabella passaggi");
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("icona.png"));
        
        Globali.db.refreshDataBase();    // Controllo i tag privi di utenti, se è stato inserito il relativo utente lo aggiorno
        
        
        query = "SELECT id_passaggio,tag,id_utente,ora,data_passaggio FROM Passaggi;";
        nomeColonne[0] = new JLabel("id_passaggio");
        nomeColonne[1] = new JLabel("tag");
        nomeColonne[2] = new JLabel("id_utente");
        nomeColonne[3] = new JLabel("Data e ora");
        tab = new Tabella();
        
        c = this.getContentPane();      
        
        c.add(tab);
        
        scrollBar=new JScrollPane(tab,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
        this.add(scrollBar);
        this.setVisible(true);
    }
   
      
    
    public class Tabella extends JPanel{
        ArrayList id_passaggi;
        ArrayList tag;
        ArrayList id_utenti;
        ArrayList orari;
        Color colore = new Color(202,227,255);
        public void paint(Graphics g){
            super.paint(g);
            repaint();
        }
        public Tabella(){
            
            loadTabella(); 
        }
        private void loadTabella(){
            rs = Globali.db.eseguiQuery(query);
           
            inizializzaArray(rs);
            
            this.setLayout(new GridLayout(id_passaggi.size(),4));
            nomeColonne[0] = new JLabel((String)id_passaggi.get(0));
            nomeColonne[0].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(0);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[1] = new JLabel((String)tag.get(0));
            nomeColonne[1].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(1);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[2] = new JLabel((String)id_utenti.get(0));
            nomeColonne[2].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(2);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[3] = new JLabel((String)orari.get(0));
            nomeColonne[3].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(3);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            for(int i = 0; i < 4; i++){
                nomeColonne[i].setOpaque(true);
                nomeColonne[i].setBackground(new Color(173, 163, 255));
                nomeColonne[i].setHorizontalAlignment(JLabel.CENTER);
                nomeColonne[i].setBorder(BorderFactory.createLineBorder(Color.black));
                nomeColonne[i].setFont(new Font("TimesRoman",Font.ROMAN_BASELINE,20));
                this.add(nomeColonne[i]);
            }
            
            for(int i = 1; i < id_passaggi.size(); i++){
                JLabel id_passaggioE = new JLabel((String)id_passaggi.get(i));
                id_passaggioE.setBorder(BorderFactory.createLineBorder(Color.black));
                id_passaggioE.setHorizontalAlignment(JLabel.CENTER);
                this.add(id_passaggioE);
                JLabel tagE = new JLabel((String)tag.get(i));
                tagE.setBorder(BorderFactory.createLineBorder(Color.black));
                tagE.setHorizontalAlignment(JLabel.CENTER);
                this.add(tagE);
                if(id_utenti.get(i)!=null){
                    JLabel id_utenteE = new JLabel((String)id_utenti.get(i));
                    id_utenteE.setBorder(BorderFactory.createLineBorder(Color.black));
                    id_utenteE.setHorizontalAlignment(JLabel.CENTER);
                    this.add(id_utenteE);
                }
                else{
                    JButton id_utenteE = new JButton("Add utente " + (String)tag.get(i));
                    final String tagAdd = (String) tag.get(i);
                    id_utenteE.addActionListener(new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            ArrayList AL = new ArrayList();
                            AL.add("");
                            AL.add(tagAdd);
                            new AggiungiUtente(AL);
                            
                        }
                    });
                    id_utenteE.setBorder(BorderFactory.createLineBorder(Color.black));
                    id_utenteE.setHorizontalAlignment(JButton.CENTER);
                    this.add(id_utenteE);
                }
                JLabel orarioE = new JLabel((String)orari.get(i));
                orarioE.setBorder(BorderFactory.createLineBorder(Color.black));
                orarioE.setHorizontalAlignment(JLabel.CENTER);
                this.add(orarioE);
            }
        }
        private void inizializzaArray(ResultSet rs){
            id_passaggi = new ArrayList();
            id_passaggi.add("id passaggio");
            tag = new ArrayList();
            tag.add("Tag");
            id_utenti = new ArrayList();
            id_utenti.add("id utente");
            orari = new ArrayList();
            orari.add("Ora e data");
            try{
                while(rs.next()){
                    id_passaggi.add(rs.getString("id_passaggio"));
                    tag.add(rs.getString("tag"));
                    id_utenti.add(rs.getString("id_utente"));
                    orari.add(rs.getString("ora") + "   " + rs.getString("data_passaggio"));
                }
            }catch(SQLException e){ }
        }
        public void ordina(int n){
            switch(n){
                case 0:{
                    temp[0] = temp[0]*-1;
                
                    if(temp[0] == 1)
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY id_passaggio ASC;";
                    else
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY id_passaggio DESC;";
                    
                }
                break;
                case 1:{
                    temp[1] = temp[1]*-1;
                
                    if(temp[1] == 1)
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY tag ASC;";
                    else
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY tag DESC;";
                }
                break;
                case 2:{
                    temp[2] = temp[2]*-1;
                
                    if(temp[2] == 1)
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY id_utente ASC;";
                    else
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY id_utente DESC;";
                    
                }
                break;
                case 3:{
                    temp[3] = temp[3]*-1;
                
                    if(temp[3] == 1)
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY data_passaggio,ora ASC;";
                    else
                        query = "SELECT id_passaggio,tag,id_utente,data_passaggio,ora FROM Passaggi ORDER BY data_passaggio,ora DESC;";
                    
                }
                break;
            }
            this.removeAll();
            loadTabella();
            this.repaint();
            this.paintAll(this.getGraphics());
        }

        
    }
}
