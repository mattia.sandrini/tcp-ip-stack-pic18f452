
package tesina;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Comunicazione extends Thread{
    private boolean termina;
    private DatagramSocket clientSocket;
    
    public boolean connesso;
    private boolean ignoraPacchettiRicevuti;   // Se true non si mette in ricezione nel metodo run (utile per la diconnessione)
    
    public enum Comandi { GIALLO_ON, VERDE_ON, ROSSO_ON, GIALLO_OFF, VERDE_OFF, ROSSO_OFF, GIALLO_BLINK_ON, GIALLO_BLINK_OFF };	
	
    public Comunicazione(){
        termina = false;
        connesso = false;
        ignoraPacchettiRicevuti = false;
        
        try{
            clientSocket = new DatagramSocket(Globali.myPortNumber);
        }catch (SocketException e) { }
        
        if(richiestaRiscontro(3, "b")){   // Tento per 3 volte la connessione, se riesco a connettermi
//           Globali.finestra.pcom.clickOnConnetti();  
            connesso = true;
	}
        else{
            clientSocket.close();   // Devo chiudere il socket, altrimenti al prossimo tentativo di connessione la porta sarà già occupata...
            termina = true;    // non inizio nemmeno il ciclo
	}
        start();
    }
    private boolean richiestaRiscontro(int tentativi, String dati){   // Tenta per n volte di stabilire una connessione
        byte[] receiveData = new byte[dati.length()];
        int i = 0;
        boolean risultato = false;
        
        
	while (i < tentativi) {
            invia(dati);   // Invio e attendo la risposta, che sarÃ  uguale a quello che Ã¨ stato inviato
            try{
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length); 
                clientSocket.setSoTimeout(2000);
		clientSocket.receive(receivePacket); // Attendo l'arrivo dei dati
		Globali.numeroPacchettiRicevuti++;   // Incremento il numero di pacchetti ricevuti
                Globali.finestra.po.aggiornaContatori();
                String datiRicevuti = new String(receivePacket.getData()).trim();
                
		if (datiRicevuti.equals(dati))            // Se ricevo la stesso cosa che ho inviato ritorno true
                    risultato = true;
                    break;
		}
		catch (SocketTimeoutException ex) {}
		catch (UnknownHostException ex) {}
		catch (IOException ex) {}
			
		i++;
	}
        
	return risultato;
    }
    
    
    public void run(){
        while(!termina){
            
            if (!ignoraPacchettiRicevuti)  // Se sono in attesa di pacchetti da leggere
                ricevi();
            
            try{
                Thread.sleep(1);
            }catch(InterruptedException e) {}
        }
    }
    public boolean chiudiConnessione() {
        
	ignoraPacchettiRicevuti = true;    // Se non esco dal ciclo infinito del thread, nel quale vengono intercettati tutti i pacchetti,
                                	   // non mi è possibile utilizzare il metodo di chiusura connessione con riscontro, perche il pacchetto
                                           // viene letto e ignorato nel metodo run...
		
	invia("d");   // invio un pacchetto dummy, così il pic mi risponderà, in modo da uscire dal metodo ricevi()...
	attendi(50);
		
	boolean risultato = richiestaRiscontro(3, "e");
		
	if (risultato) // Se la connessione è stata chiusa con successo
	{
            connesso = false;
            clientSocket.close();   // Devo chiudere il client socket, altrimenti è impossibile riaprirlo
	}
	else
	{
            connesso = true;
            ignoraPacchettiRicevuti = false;   // Mi rimetto in attesa di pacchetti
	}
        
        return risultato;
    }
    
    
    public void invia(String messaggio)
	{
		byte[] sendData; 
	    
		sendData = new byte[messaggio.length()]; 
		sendData = messaggio.getBytes();   
		
		try 
		{
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, Globali.serverIP, Globali.serverPortNumber); 
			
			clientSocket.send(sendPacket); 
			
			Globali.numeroPacchettiInviati++; // Incremento il numero di pacchetti inviati
                        Globali.finestra.po.aggiornaContatori();
		}
		catch (UnknownHostException ex) { }
		catch (IOException ex) { }
	}
    public void ricevi(){
        byte[] receiveData = new byte[12];
	try{
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length); 
            clientSocket.receive(receivePacket);// Attendo l'arrivo dei dati
            Globali.numeroPacchettiRicevuti++;
            
            String datiRicevuti = new String(receivePacket.getData()).trim();
            
            if (datiRicevuti.length() == 12) {
                LetturaRFID lettura = new LetturaRFID(datiRicevuti);
                if (lettura.calcolaChecksum()){
                    Globali.numeroPassaggi++;   // Incremento il numero di codici RFID letti
				
				
                    ArrayList rs = Globali.db.aggiungiPassaggio(datiRicevuti);
                    if(Globali.flag){
                        eseguiComando(Comandi.GIALLO_BLINK_ON);
                        Globali.finestra.pcom.passaggioSconosciuto(rs);
                    }
                    else{
                        Globali.finestra.pcom.passaggioUtente(rs);
                        illuminaVerde(1000);
                    }
                }
                else 
                    System.out.println("Errore calcolo checksum");
            }
            
            Globali.finestra.po.aggiornaContatori();
            
        }catch (UnknownHostException ex) {} catch (IOException ex) {}
    }
    
    
    public void eseguiComando(Comandi c) {
        
        switch (c) {
            case GIALLO_ON:
                richiestaRiscontro(3, "1");
            break;
            case VERDE_ON:
                richiestaRiscontro(3, "2");
            break;
            case ROSSO_ON:
                richiestaRiscontro(3, "3");
            break;
            case GIALLO_OFF:
                richiestaRiscontro(3, "4");
            break;
            case VERDE_OFF:
                richiestaRiscontro(3, "5");
            break;
            case ROSSO_OFF:
                richiestaRiscontro(3, "6");
            break;
            case GIALLO_BLINK_ON: 
                richiestaRiscontro(3, "7");
            break;
            case GIALLO_BLINK_OFF: 
                richiestaRiscontro(3, "8");
            break;
        }
    }
        
        private void attendi(int ms)
        {
            try 
            {
                this.sleep(ms);
            } 
            catch (InterruptedException e) {}
        }
        
        public void illuminaGiallo(int ms)
        {
            ignoraPacchettiRicevuti = true;     // Il metodo run ignora i pacchetti, altrimenti si rischia che intercetti i pacchetti del riscontro
            invia("d");   // invio un pacchetto dummy, così il pic mi risponderà, in modo da uscire dal metodo ricevi()...
            attendi(50);
            
            eseguiComando(Comandi.GIALLO_ON);
            attendi(ms);
            eseguiComando(Comandi.GIALLO_OFF);
            
            ignoraPacchettiRicevuti = false;   // Mi rimetto in ascolto dei pacchetti
        }
        
        public void illuminaVerde(int ms)
        {
            ignoraPacchettiRicevuti = true;     // Il metodo run ignora i pacchetti, altrimenti si rischia che intercetti i pacchetti del riscontro
            invia("d");   // invio un pacchetto dummy, così il pic mi risponderà, in modo da uscire dal metodo ricevi()...
            attendi(50);
            
            eseguiComando(Comandi.VERDE_ON);
            attendi(ms);
            eseguiComando(Comandi.VERDE_OFF);
            
            ignoraPacchettiRicevuti = false;   // Mi rimetto in ascolto dei pacchetti
        }
        
        public void illuminaRosso(int ms)
        {
            ignoraPacchettiRicevuti = true;     // Il metodo run ignora i pacchetti, altrimenti si rischia che intercetti i pacchetti del riscontro
            invia("d");   // invio un pacchetto dummy, così il pic mi risponderà, in modo da uscire dal metodo ricevi()...
            attendi(50);
            
            eseguiComando(Comandi.ROSSO_ON);
            attendi(ms);
            eseguiComando(Comandi.ROSSO_OFF);
            
            ignoraPacchettiRicevuti = false;   // Mi rimetto in ascolto dei pacchetti
        }
}

