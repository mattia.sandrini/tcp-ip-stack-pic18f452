

package tesina;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class Globali {
    public static ArrayList rsTemp;//variabile utilizzata per l'inserimento di una nuova persona
    public static DataBase db;
    public static int serverPortNumber;
    public static int myPortNumber;
    public static InetAddress serverIP;
    
    
    public static String IPServerDefault = "192.168.1.13";
    public static int PortNumberServerDefault = 33000;
    public static int MyPortNumberDefault = 50000;
    
    public static VisualizzaPassaggi vp;
    public static boolean flag;
    public static Finestra finestra;
    public static Comunicazione comunicazione;
    public static int numeroPacchettiInviati = 0, numeroPacchettiRicevuti = 0, numeroPassaggi = 0;
    public static String temp;
    public static String getData(){
        Calendar data = new GregorianCalendar();
	data.setTime(new Date());
	String testoData = data.get(Calendar.DATE) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR);
	return testoData;
    }
	
    public static String getOra(){
        Calendar data = new GregorianCalendar();
	data.setTime(new Date());
	String testoOra = data.get(Calendar.HOUR) + ":" + data.get(Calendar.MINUTE);
	return testoOra;
	}
}
