
package tesina;


public class LetturaRFID 
{
	String codiceLetto;
	byte[] codiceEffettivo;
	
	
	
	public LetturaRFID(String codice)
	{
		this.codiceLetto = codice;
		
		codiceEffettivo = ASCIItoHEX(codice);
	}
	
	
	public LetturaRFID(byte[] codice)
	{
		this.codiceLetto = new String(codice);
		
		codiceEffettivo = ASCIItoHEX(codice);
	}
	
	
	/* L'RFID invia 16 byte:
	
		1 -> STX
		10 -> Due byte per ogni numero esadecimale
		2 -> Due byte per il numero esadecimale relativo al checksum
		1 -> CR
		1 -> LF
		1 -> ETX
	*/
	public byte[] ASCIItoHEX(byte[] codice)
	{
		byte[] risultato = new byte[6];
		byte temp = 0;
		
		for (int i = 0, j = 0; i < codice.length; i++)
		{
			// Eseguo la conversione Ascii/Hex
			if ((codice[i] >= '0') && (codice[i] <= '9'))
				codice[i] -= '0';
			else if ((codice[i] >= 'A') && (codice[i] <= 'F')) 
				codice[i] += 10 - 'A';
			
			
			if (i % 2 != 0) 
			{
				// Condenso i 2 valori alfanumerici, già trasformati in cifre da 1 a 15, in un singolo valore esadecimale
				// Shiftando verso sinistra i 4 bit relativi al valore letto precedentemente
				
				risultato[j++] = (byte)(codice[i] | (temp << 4));
				
				//System.out.println("i: " + i + ";... codice[i]:" + codice[i] + ";... temp:" + (temp) + ";... temp << 4:" + (temp << 4) + ";... codice[i] | (temp << 4):" + (byte)(codice[i] | (temp << 4)));	
			} 
			else 
	 			temp = codice[i];        // Memorizzo la prima parte del numero esadecimale
		}
		
		return risultato;    // Ritorno l'array di 6 byte convertiti in esadecimale 
	}
	
	
	public byte[] ASCIItoHEX(String s)
	{
		return ASCIItoHEX(s.getBytes());
	}
	
	
	
	public boolean calcolaChecksum(byte[] codice)
	{
		byte checksum = 0;
		
		for (int i = 0; i < codice.length - 1; i++)   // l'ultimo byte non serve (è il checksum)
		{          
 			checksum ^= codice[i];       // Calcolo il checksum... (XOR)
 			
 			//System.out.println("i: " + i + ";.. checksum: " + (byte)checksum + ";... codice[i]:" + codice[i]);	
		}
		
		return codice[5] == checksum;   // Ritorna true se il checksum è corretto
	}
	
	
	public boolean calcolaChecksum()
	{
		return calcolaChecksum(codiceEffettivo);   // Ritorna true se il checksum è corretto
	}
	
	
	public String toString()
	{
		String s = "";
		
		for (byte b : codiceEffettivo)   // Stampo anche il checksum
		{
			int i = b & 0xFF;       // Il byte potrebbe risultare negativo, in questo modo ottengo il numero sempre positivo
			
			s += (i < 15) ? "0" : "";        // Se il numero esadecimale è composto da una sola cifra alfanumerica, aggiungo lo 0 davanti
			s += String.format("%1$h ", i);
		}
		
		return s;
	}
	
}








