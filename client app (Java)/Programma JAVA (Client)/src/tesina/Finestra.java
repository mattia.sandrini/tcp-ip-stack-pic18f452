
package tesina;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class Finestra extends JFrame{

    private JButton connetti;
    private Container sfondo;
    private JMenu visualizza,opzioni;
    private JMenuBar menu;
    private JMenuItem visualizza_passaggi,visualizza_registrati;
    private JMenuItem change_impostazioni,visual_impostazioni;
    public PannelloComunicazione pcom;
    private PannelloConnetti pcon;
    public PannelloOrario po;
    
    
    public Finestra(){
        this.setBounds(200, 200, 600, 400);
        this.setTitle("Monitoraggio accessi");
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        sfondo = this.getContentPane();
        
        
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("icona.png"));
        
        menu = new JMenuBar();
        menu.setBackground(new Color(221, 221, 221));
        
        this.setJMenuBar(menu);
        
        visualizza = new JMenu("Visualizza");
        menu.add(visualizza);
        
        
        visual_impostazioni = new JMenuItem("Visualizza parametri");
        visual_impostazioni.addActionListener(new AscoltaMenu());
        change_impostazioni = new JMenuItem("Opzioni comunicazione");
        change_impostazioni.addActionListener(new AscoltaMenu());
        opzioni = new JMenu("Opzioni");
        opzioni.add(change_impostazioni);
        opzioni.add(visual_impostazioni);
        
        menu.add(opzioni);
        
        
        visualizza_passaggi = new JMenuItem("Visualizza passaggi");
        visualizza_passaggi.addActionListener(new AscoltaMenu());
        visualizza.add(visualizza_passaggi);
        visualizza_registrati = new JMenuItem("Visualizza utenti");
        visualizza_registrati.addActionListener(new AscoltaMenu());
        visualizza.add(visualizza_registrati);
        
                
        pcom = new PannelloComunicazione();
        pcon = new PannelloConnetti();
        po = new PannelloOrario();
        
        sfondo.add(pcom,"Center");
        sfondo.add(pcon,"South");
        sfondo.add(po,"East");
        
        this.setVisible(true);
    }

    public class PannelloComunicazione extends JPanel{
        Color colore = new Color(173, 163, 255);
        int stato = -1;
        JLabel etichetta;
        Font f;
        JButton clear;
        JButton add,ignore;
        
        
        public PannelloComunicazione(){
            this.setDoubleBuffered(true);
            this.setBorder(new TitledBorder(new EtchedBorder(), "Stato"));
            this.setBackground(colore);
            this.setLayout(new BorderLayout());
        }
        public void paint(Graphics g){
            super.paint(g);             
            repaint();
            
            
        }
        public void clickOnConnetti(){
            
            
            if (Globali.comunicazione.connesso) {   // Se la connessione è avvenuta con successo
                connetti.setText("Disconnetti");
                this.removeAll();
                this.setLayout(new BorderLayout());
                etichetta = new JLabel("Attendi...");
                f = new Font("TimesRoman",Font.PLAIN,60);
                etichetta.setFont(f);
                this.add(etichetta,"Center");
            }
            else
                Globali.comunicazione = null;
            
            this.paintAll(this.getGraphics());
        }
        
        public void clickOnDisconnetti(){
            this.removeAll();
            
            if (Globali.comunicazione.chiudiConnessione()) {
                connetti.setText("Connetti");
                
            }
            else {
                
                JOptionPane.showMessageDialog(null, "Disconnessione non riuscita");
            }
            
            
        }
        
        public void passaggioUtente(ArrayList rs){
           
            this.removeAll();
            this.setLayout(null);
            etichetta = new JLabel("<html><h2>Codice Fiscale: "+
                                    rs.get(1) + "<br>Nome: " +
                                    rs.get(2)+" <br>Cognome: " +
                                    rs.get(3)+" <br>Tag: " +
                                    rs.get(4)+" <br>Indirizzo: " +
                                    rs.get(5)+"</h2></html>"); 
            etichetta.setBounds(20,0,300,200);
            this.add(etichetta);
            clear = new JButton("Clear");
            clear.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    clickOnConnetti();
            }});
            clear.setBounds(20,230,80,30);
            this.add(clear);
            this.paintAll(this.getGraphics());
            
        }
        public void passaggioSconosciuto(ArrayList rs){
            stato = 1;
            Globali.rsTemp = rs;
            this.removeAll();
            this.setLayout(null);
            etichetta = new JLabel("<html><h2>id_passaggio: "+
                                    rs.get(0) + "<br>Tag: " +
                                    rs.get(1)+" <br>Ora: " +
                                    rs.get(2)+"<br>Data: " +
                                    rs.get(3)+" </h2></html>"); 
            etichetta.setBounds(20,0,300,200);
            this.add(etichetta);
            
            add = new JButton("Aggiungi");
            add.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    new AggiungiUtente(Globali.rsTemp);
            }});
            
            add.setBounds(20,230,140,30);
            this.add(add);
            ignore = new JButton("Ignora");
            ignore.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    
                    Globali.comunicazione.illuminaRosso(1000);
                    
                    clickOnConnetti();
            }});
            
            ignore.setBounds(200, 230, 140, 30);
            this.add(ignore);
            this.paintAll(this.getGraphics());
            
        }
    }
    
    public class PannelloConnetti extends JPanel{
        
        public PannelloConnetti(){
            
            connetti = new JButton("Connetti");
            Connessione ascolta_connetti = new Connessione();
            connetti.addActionListener(ascolta_connetti);
            this.add(connetti);
            
            this.setBackground(new Color(105, 89, 255));
        }
    }
    

    
    private void stabilisciConnessione(ActionEvent e){
        if(e.getActionCommand().equals("Connetti")){
            
            Globali.comunicazione = new Comunicazione();
            pcom.clickOnConnetti();
        }
        else{
            
            pcom.clickOnDisconnetti();
        }
    }
    
    public class Connessione implements ActionListener{
        
        @Override
        public void actionPerformed(ActionEvent e){
            stabilisciConnessione(e);
            }
        }
    public class AscoltaMenu implements ActionListener{
        
        @Override
        public void actionPerformed(ActionEvent e){
            String control = e.getActionCommand();
            
            if(control.equals("Visualizza passaggi"))
                Globali.vp = new VisualizzaPassaggi();
            else if(control.equals("Visualizza utenti"))
                new VisualizzaUtenti();
            else if(control.equals("Opzioni comunicazione"))
                new Opzioni();
            else if(control.equals("Visualizza parametri")) {
                try {
                    String myIPAddress = "";
                    
                    // Trovo l'indirizzo IP corrispondente all'interfaccia di rete connessa col sistema del Microcontrollore
                    for (Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
                         NetworkInterface iface = ifaces.nextElement();
                         if (iface.getName().equals("eth3"))
                            for (Enumeration<InetAddress> addresses = iface.getInetAddresses(); addresses.hasMoreElements();) 
                                 myIPAddress = addresses.nextElement().getHostAddress();      
                     }

                     JOptionPane.showMessageDialog(null, "<html><h2>"
                        + "My Port Number: "+Globali.myPortNumber+"<br>"
                        + "My IP Address: " + myIPAddress +"<br>"
                        + "Server Port Number: "+Globali.serverPortNumber+"<br>"
                        + "Server IP Address: "+Globali.serverIP+"<br></h2></html>");
                } catch (SocketException ex) { }
            }
        }  
    }
        
    
    
}