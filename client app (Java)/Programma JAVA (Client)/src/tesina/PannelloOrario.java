
package tesina;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


    public class PannelloOrario extends JPanel{
        
        Color colore;
        JLabel etichetta;
        Thread t;
        String spazi3 = "&nbsp;&nbsp;&nbsp;",spazi5 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        
        public PannelloOrario(){
            
            this.colore = new Color(191, 173, 255);
            this.setBorder(new TitledBorder(new EtchedBorder(), "Dettagli comunicazione"));
            this.setDoubleBuffered(true);
            this.setBackground(colore);
            this.setLayout(new BorderLayout());
            ricaricaPannello();
            t = new Thread(new MioThread(),"Thread gestione ora");
            t.start();
        }
        public void paint(Graphics g){
            super.paint(g);
            repaint();
            
            
        }
        public void aggiornaContatori(){
            ricaricaPannello();
        }

        public void ricaricaPannello(){
            this.removeAll();
            String testo1 = "<html><h2>"+spazi5+Globali.getOra()+"<br><br>"+spazi3+Globali.getData()+spazi3+"</h2>";
            String testo2 = "<h3>&nbsp;Pacchetti trasmessi: "+Globali.numeroPacchettiInviati+"&nbsp;<br><br>&nbsp;Pacchetti Ricevuti:"+Globali.numeroPacchettiRicevuti+"&nbsp;<br><br>&nbsp;Passaggi:"+Globali.numeroPassaggi+"&nbsp;</h3></html>";
            etichetta = new JLabel(testo1+testo2);
            this.add(etichetta,"Center");
            this.paintAll(this.getGraphics());
            
        }
    public class MioThread implements Runnable{
        public void run(){
            while(true){
                ricaricaPannello();
                try{
                    Thread.sleep(10000);
                }catch(InterruptedException e){}
            }
        }
        
    }
    }