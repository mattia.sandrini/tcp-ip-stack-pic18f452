
package tesina;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class VisualizzaUtenti extends JFrame{
    private Tabella tab;
    private JLabel[] nomeColonne = new JLabel[7];
    private String query;
    private ResultSet rs;
    private JScrollPane scrollBar;
    private int[] temp = {1,1,1,1,1,1};
    private Container c;
    Color col = Color.blue;
    public VisualizzaUtenti(){
        this.setBounds(200,200,950,300);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setTitle("Tabella utenti");
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("icona.png"));
        
        
        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti;";
        
        
        
        tab = new Tabella();
        
        c = this.getContentPane();      
        
        c.add(tab);
        
        scrollBar=new JScrollPane(tab,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
        this.add(scrollBar);
        this.setVisible(true);
    }
   
      
    
    public class Tabella extends JPanel{
        ArrayList id_utenti;
        ArrayList codFiscali;
        ArrayList tag;
        ArrayList nomi;
        ArrayList cognomi;
        ArrayList indirizzi;
        public void paint(Graphics g){
            super.paint(g);
            repaint();
        }
        public Tabella(){
            
            loadTabella(); 
        }
        private void loadTabella(){
            rs = Globali.db.eseguiQuery(query);
           
            inizializzaArray(rs);
            
            this.setLayout(new GridLayout(id_utenti.size(),7));
            
            nomeColonne[0] = new JLabel((String)id_utenti.get(0));
            nomeColonne[0].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(0);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[1] = new JLabel((String)codFiscali.get(0));
            nomeColonne[1].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(1);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[2] = new JLabel((String)tag.get(0));
            nomeColonne[2].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(2);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });

            nomeColonne[3] = new JLabel((String)nomi.get(0));
            nomeColonne[3].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(3);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });
            
            nomeColonne[4] = new JLabel((String)cognomi.get(0));
            nomeColonne[4].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(4);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });
            
            nomeColonne[5] = new JLabel((String)indirizzi.get(0));
            nomeColonne[5].addMouseListener(new MouseListener(){
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ordina(5);
                    }
                    @Override public void mousePressed(MouseEvent e) {} 
                    @Override public void mouseReleased(MouseEvent e) {} 
                    @Override public void mouseEntered(MouseEvent e) {} 
                    @Override public void mouseExited(MouseEvent e) {}
                });
            nomeColonne[6] = new JLabel("Elimina");
            
            for(int i = 0; i < 7; i++){
                nomeColonne[i].setOpaque(true);
                nomeColonne[i].setBackground(new Color(173, 163, 255));
                nomeColonne[i].setHorizontalAlignment(JLabel.CENTER);
                nomeColonne[i].setBorder(BorderFactory.createLineBorder(Color.black));
                nomeColonne[i].setFont(new Font("TimesRoman",Font.ROMAN_BASELINE,20));
                this.add(nomeColonne[i]);
            }
            for(int i = 1; i < id_utenti.size(); i++){
                JLabel id_utenteE = new JLabel((String)id_utenti.get(i));
                id_utenteE.setBorder(BorderFactory.createLineBorder(Color.black));
                id_utenteE.setHorizontalAlignment(JLabel.CENTER);
                this.add(id_utenteE);
                JLabel CF = new JLabel((String)codFiscali.get(i));
                CF.setBorder(BorderFactory.createLineBorder(Color.black));
                CF.setHorizontalAlignment(JLabel.CENTER);
                this.add(CF);
                JLabel tagE = new JLabel((String)tag.get(i));
                tagE.setBorder(BorderFactory.createLineBorder(Color.black));
                tagE.setHorizontalAlignment(JLabel.CENTER);
                this.add(tagE);
                JLabel nomeE = new JLabel((String)nomi.get(i));
                nomeE.setBorder(BorderFactory.createLineBorder(Color.black));
                nomeE.setHorizontalAlignment(JLabel.CENTER);
                this.add(nomeE);
                JLabel cognomeE = new JLabel((String)cognomi.get(i));
                cognomeE.setBorder(BorderFactory.createLineBorder(Color.black));
                cognomeE.setHorizontalAlignment(JLabel.CENTER);
                this.add(cognomeE);
                JLabel indirizzoE = new JLabel((String)indirizzi.get(i));
                indirizzoE.setBorder(BorderFactory.createLineBorder(Color.black));
                indirizzoE.setHorizontalAlignment(JLabel.CENTER);
                this.add(indirizzoE);
                JButton elimina = new JButton("Canc");
                final String tagDel = (String) tag.get(i);
                elimina.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String query = "DELETE FROM Utenti WHERE tag='"+tagDel+"';";
                        Globali.db.eseguiQuery(query);
                        ordina(0);
                    }
                });
                this.add(elimina);
            }
        }
        private void inizializzaArray(ResultSet rs){
            id_utenti = new ArrayList();
            id_utenti.add("id utente");
            codFiscali = new ArrayList();
            codFiscali.add("Codice Fiscale");
            tag = new ArrayList();
            tag.add("Tag");
            nomi = new ArrayList();
            nomi.add("nome");
            cognomi = new ArrayList();
            cognomi.add("Cognome");
            indirizzi = new ArrayList();
            indirizzi.add("indirizzo");
            try{
                while(rs.next()){
                    id_utenti.add(rs.getString("id_utente"));
                    codFiscali.add(rs.getString("codFiscale"));
                    tag.add(rs.getString("tag"));
                    nomi.add(rs.getString("nome"));
                    cognomi.add(rs.getString("cognome"));
                    indirizzi.add(rs.getString("indirizzo"));
                }
            }catch(SQLException e){}
        }
        public void ordina(int n){
            switch(n){
                case 0:{
                    temp[0] = temp[0]*-1;
                
                    if(temp[0] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY id_utente ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY id_utente DESC;";
                    
                }
                break;
                case 1:{
                    temp[1] = temp[1]*-1;
                
                    if(temp[1] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY codFiscale ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY codFiscale DESC;";
                }
                break;
                case 2:{
                    temp[2] = temp[2]*-1;
                
                    if(temp[2] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY tag ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY tag DESC;";
                    
                }
                break;
                case 3:{
                    temp[3] = temp[3]*-1;
                
                    if(temp[3] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY nome ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY nome DESC;";
                    
                }
                break;
                case 4:{
                    temp[4] = temp[4]*-1;
                
                    if(temp[4] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY cognome ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY cognome DESC;";
                    
                }
                break;
                case 5:{
                    temp[5] = temp[5]*-1;
                
                    if(temp[5] == 1)
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY indirizzo ASC;";
                    else
                        query = "SELECT id_utente,codFiscale,tag,nome,cognome,indirizzo FROM Utenti ORDER BY indirizzo DESC;";
                    
                }
                break;
            }
            this.removeAll();
            loadTabella();
            this.repaint();
            this.paintAll(this.getGraphics());
        }

        
    }
}
